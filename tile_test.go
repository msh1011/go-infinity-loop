package main

import (
	"testing"
)

func TestGetPoints(t *testing.T) {
	tt := []struct {
		name string
		ty   byte
		exp  []byte
	}{
		{
			name: "end",
			ty:   1,
			exp:  []byte{1, 2, 4, 8},
		},
		{
			name: "line",
			ty:   2,
			exp:  []byte{5, 10, 5, 10},
		},
		{
			name: "corner",
			ty:   3,
			exp:  []byte{3, 6, 12, 9},
		},
		{
			name: "t_intersect",
			ty:   4,
			exp:  []byte{7, 14, 13, 11},
		},
		{
			name: "x_intersect",
			ty:   5,
			exp:  []byte{15, 15, 15, 15},
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			tile := Tile{tc.ty, 0, false}
			for i, e := range tc.exp {
				val := tile.getPointsDir(byte(i))
				if val != e {
					t.Fatalf("Incorrect value: exp %d, got %d", e, val)
				}
			}
		})
	}
}

func TestCanConnect(t *testing.T) {
	tt := []struct {
		name string
		ty   Tile
		exp  []int
	}{
		{
			"end_up",
			Tile{1, 0, true},
			[]int{2, 0, 0, 0},
		},
		{
			"end_right",
			Tile{1, 1, true},
			[]int{0, 2, 0, 0},
		},
		{
			"line_up",
			Tile{2, 0, true},
			[]int{2, 0, 2, 0},
		},
		{
			"line_right",
			Tile{2, 1, true},
			[]int{0, 2, 0, 2},
		},
		{
			"line_right_inv",
			Tile{2, 1, false},
			[]int{1, 1, 1, 1},
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			for i, d := range []byte{UP, RIGHT, DOWN, LEFT} {
				val := tc.ty.canConnect(d)
				if val != tc.exp[i] {
					t.Fatalf("Incorrect value: exp %d, got %d", tc.exp[i], val)
				}
			}
		})
	}
}
