package main

import (
	"fmt"
	"math"
)

// State holds a snapshot of a world
type State struct {
	grid  [][]Tile
	nRows int
	nCols int
	size  int
}

func (s *State) copyState() *State {
	s1 := &State{}
	world := make([][]Tile, len(s.grid))
	for a, i := range s.grid {
		world[a] = make([]Tile, len(i))
		for b, j := range i {
			c := Tile{
				TYPE:   j.TYPE,
				ROT:    j.ROT,
				SOLVED: j.SOLVED,
			}
			world[a][b] = c
		}
	}
	s1.grid = world
	s1.nCols = s.nCols
	s1.nRows = s.nRows
	return s1
}

const (
	UP    byte = 1
	RIGHT byte = 1 << 1
	DOWN  byte = 1 << 2
	LEFT  byte = 1 << 3
)

var SIZES = []int{115, 118, 121, 124, 127, 130}

var strToType = map[string]byte{
	"end":         1,
	"line":        2,
	"corner":      3,
	"t_intersect": 4,
	"x_intersect": 5,
}

type LoadedLvl struct {
	state   *State
	minX    int
	minY    int
	size    int
	objects int
}

func loadLvl(lvl string) (*State, int, int, int, error) {
	bestL := 0
	var level *LoadedLvl
	c1 := make(chan *LoadedLvl)
	getObjectsAtSize := func(s int) {
		l, _ := loadLvlSize(lvl, s)
		c1 <- l
	}
	for _, s := range SIZES {
		go getObjectsAtSize(s)
	}
	for _ = range SIZES {
		obj := <-c1
		if obj.objects > bestL {
			bestL = obj.objects
			level = obj
		}
	}
	return level.state, level.minX, level.minY, level.size, nil
}

func loadLvlSize(lvl string, cellSize int) (*LoadedLvl, error) {
	Objs := GetObjectsInLevel(lvl, cellSize)
	numElements := 0
	if len(Objs) == 0 {
		return nil, fmt.Errorf("No Objects detected")
	}
	MinX := 1<<31 - 1
	MaxX := 0
	MinY := 1<<31 - 1
	MaxY := 0
	for _, elem := range Objs {
		if elem.X < MinX {
			MinX = elem.X
		} else if elem.X > MaxX {
			MaxX = elem.X
		}
		if elem.Y < MinY {
			MinY = elem.Y
		} else if elem.Y > MaxY {
			MaxY = elem.Y
		}
	}
	sizeX := (MaxX - MinX + cellSize) / cellSize
	sizeY := (MaxY - MinY + cellSize) / cellSize
	ret := make([][]Tile, sizeY)
	exp := make([][]Object, sizeY)
	confMax := make([][]float32, sizeY)
	counted := make([][]bool, sizeY)
	for i := range ret {
		ret[i] = make([]Tile, sizeX)
		exp[i] = make([]Object, sizeX)
		confMax[i] = make([]float32, sizeX)
		counted[i] = make([]bool, sizeX)
		for j := range ret[i] {
			ret[i][j] = Tile{6, 0, true}
			exp[i][j] = Object{
				X: MinX + (j * cellSize),
				Y: MinY + (i * cellSize),
			}
		}
	}
	for _, elem := range Objs {
		lx, ly := elem.X-MinX, elem.Y-MinY
		nx, ny := lx/cellSize, ly/cellSize
		if confMax[ny][nx] < elem.conf {
			xDist := math.Pow(float64(exp[ny][nx].X-elem.X), 2)
			yDist := math.Pow(float64(exp[ny][nx].Y-elem.Y), 2)
			dist := math.Sqrt(xDist + yDist)
			if dist > 50 {
				continue
			}
			confMax[ny][nx] = elem.conf
			if !counted[ny][nx] {
				counted[ny][nx] = true
				numElements++
			}

			ret[ny][nx] = Tile{strToType[elem.name], byte(elem.dir), false}
		}
	}
	st := &State{ret, sizeX, sizeY, cellSize}
	return &LoadedLvl{st, MinX, MinY, cellSize, numElements}, nil
}

func (s State) getValidDirs(x, y int) []byte {
	t := &s.grid[x][y]
	left := EMTPY
	if y > 0 {
		left = &s.grid[x][y-1]
	}
	right := EMTPY
	if y < s.nRows-1 {
		right = &s.grid[x][y+1]
	}
	down := EMTPY
	if x < s.nCols-1 {
		down = &s.grid[x+1][y]
	}
	up := EMTPY
	if x > 0 {
		up = &s.grid[x-1][y]
	}
	valid := []byte{}
	for _, i := range t.getPosibleOr() {
		if isValidOr(i, t, up, right, down, left) {
			valid = append(valid, i)
		}
	}
	return valid
}

func (s State) complete() bool {
	for x := range s.grid {
		for y := range s.grid[x] {
			if !s.grid[x][y].SOLVED {
				return false
			}
		}
	}
	return true
}

func isValidOr(i byte, t, up, right, down, left *Tile) bool {
	needed := t.getPointsDir(i)
	testDir := func(dir byte, con int) bool {
		if con == CANT_CONNECT && needed&dir != 0 {
			return true
		}
		if con == MUST_CONNECT && needed&dir == 0 {
			return true
		}
		return false
	}
	if testDir(UP, up.canConnect(DOWN)) {
		return false
	}
	if testDir(RIGHT, right.canConnect(LEFT)) {
		return false
	}
	if testDir(DOWN, down.canConnect(UP)) {
		return false
	}
	if testDir(LEFT, left.canConnect(RIGHT)) {
		return false
	}
	return true
}

func (s State) getActions(s1 *State) (act [][]byte) {
	act = make([][]byte, s.nCols)
	for i := range act {
		act[i] = make([]byte, s.nRows)
		for j := range act[i] {
			tmp := s.grid[i][j].ROT
			for tmp != s1.grid[i][j].ROT {
				act[i][j]++
				tmp = (tmp + 1) % 4
			}
			if s.grid[i][j].TYPE == 2 && act[i][j] == 3 {
				act[i][j] -= 2
			} else if s.grid[i][j].TYPE == 5 || s.grid[i][j].TYPE == 6 {
				act[i][j] = 0
			}
		}
	}
	return
}

func (s State) printAt(xoff, yoff int) {
	for x, i := range s.grid {
		for y, j := range i {
			fmt.Printf("\033[%d;%dH", xoff+x, yoff+y)
			fmt.Print(j.String())
		}
	}
}
func (s State) String() string {
	ret := ""
	for _, i := range s.grid {
		for _, j := range i {
			ret += j.String()
		}
		ret += "\n"
	}
	return ret[:len(ret)-1]
}

func (state *State) solve() *State {
	s := state.copyState()
	foundMove := false
	solved := true
	for {
		foundMove = false
		solved = true
		for x := range s.grid {
			for y := range s.grid[x] {
				if s.grid[x][y].SOLVED {
					continue
				}
				validOrientations := s.getValidDirs(x, y)
				if len(validOrientations) == 1 {
					s.grid[x][y].ROT = validOrientations[0]
					s.grid[x][y].SOLVED = true
					foundMove = true
				} else {
					solved = false
				}
			}
		}
		if !foundMove {
			break
		}
	}
	if solved {
		return s
	}

	for x := range s.grid {
		for y := range s.grid[x] {
			if s.grid[x][y].SOLVED {
				continue
			}
			validOrientations := s.getValidDirs(x, y)
			for _, i := range validOrientations {
				nState := s.copyState()
				nState.grid[x][y].ROT = i
				nState.grid[x][y].SOLVED = true
				nState = nState.solve()
				if nState != nil {
					return nState
				}
			}
		}
	}
	return nil
}
