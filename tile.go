package main

import "math/rand"

// Tile holds data on one tile
type Tile struct {
	TYPE   byte
	ROT    byte
	SOLVED bool
}

// EMPTY is used as a blank space in the grid
var EMTPY = &Tile{6, 0, true}
var CANT_CONNECT = 0
var CAN_CONNECT = 1
var MUST_CONNECT = 2

func (c Tile) getPosibleOr() []byte {
	switch c.TYPE {
	case 1:
		return []byte{0, 1, 2, 3}
	case 2:
		return []byte{0, 1}
	case 3:
		return []byte{0, 1, 2, 3}
	case 4:
		return []byte{0, 1, 2, 3}
	case 5:
		return []byte{0}
	}
	return []byte{}
}

func (c Tile) mix() {
	if c.TYPE == 5 {
		return
	}
	if c.TYPE == 6 {
		return
	}
	c.SOLVED = false
	for i := rand.Intn(4); i != 0; i-- {
		c.roate()
	}

}

func (c Tile) roate() {
	c.ROT = (c.ROT + 1) % 4
}

func (c *Tile) getPoints() byte {
	return c.getPointsDir(c.ROT)
}

func (c *Tile) getPointsDir(rot byte) byte {
	switch c.TYPE {
	case 1:
		return 1 << rot
	case 2:
		return 1<<rot | 1<<((rot+2)%4)
	case 3:
		return 1<<rot | 1<<((rot+1)%4)
	case 4:
		return 1<<rot | 1<<((rot+1)%4) | 1<<((rot+2)%4)
	case 5:
		return 15
	}
	return 0
}

func (c Tile) canConnect(dir byte) int {
	if c.SOLVED {
		if c.needs(dir) {
			return MUST_CONNECT
		} else {
			return CANT_CONNECT
		}
	}
	return CAN_CONNECT
}

func (c Tile) needs(dir byte) bool {
	if c.SOLVED {
		needed := c.getPoints()
		return (dir & needed) != 0
	}
	return false
}

func (c Tile) String() string {
	return printD[c.TYPE][c.ROT]
}

var printD = map[byte]map[byte]string{
	1: {
		0: "\xE2\x95\xB9",
		1: "\xE2\x95\xBA",
		2: "\xE2\x95\xBB",
		3: "\xE2\x95\xB8",
	},
	2: {
		0: "\xE2\x94\x82",
		1: "\xE2\x94\x80",
		2: "\xE2\x94\x82",
		3: "\xE2\x94\x80",
	},
	3: {
		0: "\xE2\x94\x94",
		1: "\xE2\x94\x8C",
		2: "\xE2\x94\x90",
		3: "\xE2\x94\x98",
	},
	4: {
		0: "\xE2\x94\x9C",
		1: "\xE2\x94\xAC",
		2: "\xE2\x94\xA4",
		3: "\xE2\x94\xB4",
	},
	5: {
		0: "\xE2\x94\xBC",
		1: "\xE2\x94\xBC",
		2: "\xE2\x94\xBC",
		3: "\xE2\x94\xBC",
	},
	6: {
		0: " ",
		1: " ",
		2: " ",
		3: " ",
	},
}
