package main

import (
	"encoding/json"
	"image"
	"io/ioutil"
	"path/filepath"
	"strings"

	"gocv.io/x/gocv"
)

// Object is used to store data about what gocv sees
type Object struct {
	X    int
	Y    int
	name string
	dir  int
	size int
	conf float32
}

type optionNode struct {
	Filename    string `json:"filePath"`
	Orientation []int  `json:"orientation"`
}

type options struct {
	Files []optionNode `json:"files"`
}

func transform(obj *gocv.Mat) {
	data := obj.DataPtrUint8()
	sum := 0
	for _, i := range data {
		sum += int(i)
	}
	avg := uint8(sum / len(data))
	for x := range data {
		if data[x] > avg {
			data[x] = 255
		} else {
			data[x] = 0
		}
	}
}

func findShapeCords(level, obj gocv.Mat, name string, dir int) (ret []Object) {
	res := gocv.NewMat()
	mask := gocv.NewMat()

	gocv.MatchTemplate(level, obj, &res, 5, mask)

	data, _ := res.DataPtrFloat32()
	for x, i := range data {
		if i > .9 {
			MPx := x % (res.Cols())
			MPy := x / (res.Cols())
			ret = append(ret, Object{MPx, MPy, name, dir, obj.Cols(), i})
		}
	}
	return
}

// GetObjectsInLevel Use Template Matching to find all TileObjects in a screenshot
func GetObjectsInLevel(levelPath string, size int) (ret []Object) {
	level := gocv.IMRead(levelPath, 0)

	file, _ := ioutil.ReadFile("data.json")
	data := options{}
	json.Unmarshal([]byte(file), &data)

	for _, d := range data.Files {
		name := strings.TrimSuffix(d.Filename, filepath.Ext(d.Filename))
		obj1 := gocv.IMRead("tiles/"+d.Filename, 0)
		obj := gocv.NewMat()
		gocv.Resize(obj1, &obj, image.Point{size, size}, 0, 0, gocv.InterpolationDefault)
		for _, or := range d.Orientation {
			rt := obj.Clone()
			if or != 0 {
				gocv.Rotate(obj, &rt, gocv.RotateFlag(or-1))
			}
			ret = append(ret, findShapeCords(level, rt, name, or)...)
		}
	}
	return
}
