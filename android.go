package main

import (
	"fmt"
	"image"
	_ "image/png"
	"log"
	"os"
	"os/exec"
	"strings"
	"time"
)

func runCommand(cmd string) {
	words := strings.Fields(cmd)
	exec.Command(words[0], words[1:]...).Run()
}

func startApp() {
	runCommand("adb shell am start com.balysv.loop/com.balysv.loop.GameActivity")
}

func screenShot() {
	runCommand("adb shell screencap -p /sdcard/screencap.png")
	runCommand("adb pull /sdcard/screencap.png /tmp/screen.png")
}

func tapScreen(x, y int) {
	runCommand(fmt.Sprintf("adb shell input tap %d %d", x, y))
}

func tapScreenMulti(x, y int, t byte) {
	for i := 0; i < int(t); i++ {
		runCommand(fmt.Sprintf("adb shell input tap %d %d", x, y))
	}
}

func dragDown() {
	runCommand("adb shell input swipe 200 200 200 2000 300")
}

func record() (*exec.Cmd, string) {
	file := fmt.Sprintf("%d.mp4", time.Now().Unix())
	cmdS := "adb shell screenrecord /sdcard/" + file
	words := strings.Fields(cmdS)
	cmd := exec.Command(words[0], words[1:]...)
	cmd.Start()
	return cmd, file
}

func save(s string) {
	runCommand("adb pull /sdcard/" + s + " /mnt/d/InfinityLoop/")
	runCommand("adb shell rm -f /sdcard/" + s)
}

func startGame() {
	startApp()
	screenShot()
	file, err := os.Open("/tmp/screen.png")
	defer file.Close()
	if err != nil {
		log.Println(err)
	}
	image, _, err := image.DecodeConfig(file)
	if err != nil {
		log.Println(err)
	}
	tapScreen(image.Width/2, image.Height/3)
}
