package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"sync"
	"time"
)

var (
	// Out is used as a package iobuffer
	Out *log.Logger = log.New(os.Stdout, "", 0)
	// Debug is used as easily slienced iobuffer
	Debug *log.Logger = log.New(os.Stdout, "DEBUG: ",
		log.Ldate|log.Ltime|log.Lshortfile)
)

func debug(state *State) {
	var x, y int
	_, err := fmt.Scanf("%d %d", &x, &y)
	if err != nil {
		return
	}
	Out.Printf("%s: %#v", state.grid[x][y], state.grid[x][y])
	debug(state)
}

func doActions(actions [][]byte, minX, minY, size int) {
	var wg sync.WaitGroup
	callTouch := func(tx, ty int, t byte) {
		defer wg.Done()
		tapScreenMulti(tx, ty, t)
	}
	for i := range actions {
		for j, e := range actions[i] {
			if e == 0 {
				continue
			}
			tapX := minX + (j * size) + size/3
			tapY := minY + (i * size) + size/3
			wg.Add(1)
			go callTouch(tapX, tapY, e)
		}
	}
	wg.Wait()
}

func main() {
	saveDir := os.Getenv("SAVE_DIR")
	var file string
	var process *exec.Cmd
	for {
		screenShot()
		start := time.Now()
		level, minX, minY, size, err := loadLvl("/tmp/screen.png")
		if err != nil {
			Out.Println(err)
			break
		}
		elapsed := time.Since(start)
		reference := level.copyState()
		fmt.Print("\033[2J")
		reference.printAt(5, 5)
		Out.Printf("\033[20;0HLoaded in %s        \n", elapsed)
		start = time.Now()
		level = level.solve()
		if level == nil {
			continue
		}
		elapsed = time.Since(start)
		level.printAt(5, 30)
		Out.Printf("\033[21;0HSolved in %s        \n", elapsed)
		actions := reference.getActions(level)
		if saveDir != "" {
			process, file = record()
		}
		doActions(actions, minX, minY, size)
		time.Sleep(1 * time.Second)
		if saveDir != "" {
			process.Process.Kill()
			time.Sleep(1 * time.Second)
			save(file)
		}
		dragDown()
		time.Sleep(2 * time.Second)
	}
}
